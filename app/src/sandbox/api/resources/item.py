from flask_restful import Resource, request
from flask_jwt_extended import jwt_required, get_jwt, get_jwt_identity
from sandbox.models.item import ItemModel
from sandbox.api.schemas.item import ItemSchema
from .messages import *


item_schema = ItemSchema()
item_list_schema = ItemSchema(many=True)


class Item(Resource):
    #TODO: return back token

    # @jwt_required()
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item_schema.dump(item)
        return {'message': NOT_FOUND_ERROR.format(self.__class__.__name__)}, 404

    #TODO: return back token

    # @jwt_required(fresh=True)
    def post(self, name):
        if ItemModel.find_by_name(name):
            return {'message': INSTANCE_EXISTS.format(name)}, 400

        row_item = request.get_json()
        row_item['name'] = name

        item = item_schema.load(row_item)

        try:
            item.save_to_db()
        except:
            return {"message": CREATE_ERROR}, 500

        return item_schema.dump(item), 201

    #TODO: return back token

    # @jwt_required()
    def delete(self, name):
        # claims = get_jwt()
        # if not claims['is_admin']:
        #     return {'message': NOT_AUTHORIZED}, 401
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {'message': DELETED.format(self.__class__.__name__)}
        return {'message': NOT_FOUND_ERROR.format(self.__class__.__name__)}, 404

    def put(self, name):
        row_item = request.get_json()
        item = ItemModel.find_by_name(name)

        if item:
            item.price = row_item['price']
        else:
            row_item['name'] = name
            item = item_schema.load(row_item)

        item.save_to_db()

        return item_schema.dump(item), 200


class ItemList(Resource):
    #TODO: return back token

    # @jwt_required(optional=True)
    def get(self):
        # user_id = get_jwt_identity()
        items = item_list_schema.dump(ItemModel.find_all())
        # if user_id:
        return {'items': items}, 200
        # return {'items': [item['name'] for item in items]}, 200
