import React, {useContext} from 'react';
import {apiContext} from "../../context/api/apiContext";


const CategoryControls = ({category}) => {

    const {deleteCategory} = useContext(apiContext)

    const onDelete = () => {
        deleteCategory(category.name)
    }

    return(
        <>
            <button onClick={onDelete} className={'btn btn-danger'}>
                delete
            </button>
        </>
    )
}

export default CategoryControls