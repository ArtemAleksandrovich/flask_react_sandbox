import React from 'react'
import ItemControls from "./ItemControls";


const ItemThumbnail = ({item}) => {

    return(
        <div className={"card"}>
            <div className="card-body">
                <h5 className="card-title">{item.name}</h5>
                <p><strong>{`Item id: ${item.id}`}</strong></p>
                <ItemControls itemName={item.name}/>
            </div>
        </div>
    )
}

export default ItemThumbnail