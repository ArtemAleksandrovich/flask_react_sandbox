import React, {useContext, useEffect, Fragment} from 'react'
import {apiContext} from "../../context/api/apiContext"
import Loader from "../ui/Loader";
import {Link} from "react-router-dom";
import ItemThumbnail from "../items/ItemThumbnail";

const Category = ({match}) => {
    const {category, loading, getCategory} = useContext(apiContext)
    const categoryName = match.params.name


    useEffect(() => {
         getCategory(categoryName)
    }, [])

    if (loading) {
        return <Loader/>
    }

    const {name, id, items} = category

    return(
        <Fragment>
            <Link to="/info" className="btn btn-link">Back to INFO</Link>
            <br/><hr/><br/>
            <div className="card-body">
                <h1 className="card-title">{name}</h1>
                <p><strong>{`Category id: ${id}`}</strong></p>
                { items
                  ? items.map(item => (<div key={item.id} className="col-sm-4 mb-4"><ItemThumbnail item={item}/></div>))
                  : null
                }
            </div>
        </Fragment>
    )
}

export default Category