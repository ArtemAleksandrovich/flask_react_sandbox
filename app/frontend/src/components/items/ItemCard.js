import React from 'react';

const ItemCard = ({item}) => {
    return(
            <div className="card">
                <h1 className="card-title">{item.name}</h1>
                <p><strong>{`item id: ${item.id}`}</strong></p>
                <p><strong>{`category id: ${item.category_id}`}</strong></p>
                <p><strong>{`price: ${item.price}`}</strong></p>
            </div>
    )
}

export default ItemCard