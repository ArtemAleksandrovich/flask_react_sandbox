import React, {useContext, useState, useEffect} from 'react'
import {alertContext} from "../../context/alert/alertContext";
import {apiContext} from "../../context/api/apiContext";

const FindItem = () => {
    const [value, setValue] = useState('')
    const alert = useContext(alertContext)
    const {clearItem, getItem} = useContext(apiContext)

    const onSubmit = (event) => {
       if (event.key !== 'Enter') {
           return
       }

       clearItem()

       if (value.trim()) {
           alert.hide()
           getItem(value.trim())
       } else {
           alert.show('Enter item name for search')
       }


    }

    return(
        <div className={'form-group'}>
            <input
                type="text"
                className={'form-control'}
                placeholder={'Enter item name'}
                onChange={event => setValue(event.target.value)}
                onKeyPress={onSubmit}
            />
        </div>
    )
}

export default FindItem