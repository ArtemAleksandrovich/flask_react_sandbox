import React, {useContext, useEffect} from 'react';
import {formContext} from "../../context/form/formContext";
import {apiContext} from "../../context/api/apiContext";
import {alertContext} from "../../context/alert/alertContext";
import {Link} from "react-router-dom";


const AddCategory = () => {
    const {handleInput, addCategory, categoryFormErrors, response, clearResponse, categoryName,} = useContext(formContext)
    const {clearCategories} = useContext(apiContext)
    const alert = useContext(alertContext)

    const onChange = event => {
        handleInput(event)
    }

    useEffect(() => {
        if (response) {
            alert.show(response, 'warning')
            clearResponse()
            clearCategories()
        }

    }, [categoryName, response, categoryFormErrors.categoryName])


    const submitHandler = event => {
        event.preventDefault()
        addCategory()
    }

    // debugger
    const isEnabled = !Boolean(categoryFormErrors.categoryName)

    return (
        <>
            <Link to="/info" className="btn btn-link">Back to INFO</Link>
            <br/>
            <hr/>
            <br/>
            <div>
                <form className="demoForm" onSubmit={submitHandler}>
                    <h2>Add category</h2>
                    <div className="form-group">
                        <label>Category name</label>
                        <input
                            placeholder={'Enter category name (should contains letters, numbers or "-")'}
                            type="text"
                            className={`form-control ${categoryFormErrors.categoryName ? "error" : ""}`}
                            name="categoryName"
                            onChange={onChange}
                            value={categoryName}
                        />
                        <span style={{color: 'red'}}> {categoryFormErrors.categoryName || ""}</span>
                    </div>
                    <br/>
                    <button disabled={!isEnabled || !categoryName} type="submit" className="btn btn-success">ADD CATEGORY</button>
                </form>
            </div>
        </>
    )
}

export default AddCategory