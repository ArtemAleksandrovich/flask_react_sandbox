import React from 'react'
import {Link} from "react-router-dom";
import CategoryControls from "./CategoryControls";


const CategoryThumbnail = ({category}) => {

    return(
        <div className={"card"}>
            <div className="card-body">
                <h5 className="card-title">{category.name}</h5>
                <p><strong>{`Category id: ${category.id}`}</strong></p>
                <Link to={`/category/${category.name}`} className={'btn btn-primary'}>view category</Link>
                <CategoryControls category={category}/>
            </div>
        </div>
    )
}

export default CategoryThumbnail