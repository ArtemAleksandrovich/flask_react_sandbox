from flask import Flask
from sandbox.config import Config


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    import sandbox.models
    from sandbox.models.db import db, Base, engine
    db.init_app(app)
    Base.metadata.create_all(engine)
    from sandbox.ma import ma
    ma.init_app(app)
    from .api import api_blueprint
    app.register_blueprint(api_blueprint)
    from sandbox.extensions import jwt
    jwt.init_app(app)
    return app