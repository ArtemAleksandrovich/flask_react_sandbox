from sandbox.models.db import db


class SaveDeleteMixin(object):
    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()


class FindInstanceMixin(object):

    @classmethod
    def find_by_name(cls, name: str):
        if hasattr(cls, 'name'):
            return cls.query.filter_by(name=name).first()
        return cls.query.filter_by(username=name).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()
