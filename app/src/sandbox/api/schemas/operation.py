from sandbox.ma import ma
from sandbox.models.operation import OperationModel


class OperationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = OperationModel
        load_only = ("user",)
        dump_only = ("id", "timestamp", "answer")
        include_fk = True
        load_instance = True



