from sandbox.models.db import Base, db
from datetime import datetime
from .mixin import SaveDeleteMixin
from typing import List


class OperationModel(Base, db.Model, SaveDeleteMixin):
    __tablename__ = "operations"

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    requested_operation = db.Column(db.Text, nullable=False)
    answer = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = db.relationship("UserModel", back_populates="operations")


    @classmethod
    def find_operations(cls, user_id: int, operation=None) -> List["OperationModel"]:
        return (
            cls.query.filter_by(user_id=user_id).all()
            if not operation
            else cls.query.filter_by(
                user_id=user_id, requested_operation=operation
            ).all()
        )
