from sandbox.api.resources.item import Item, ItemList
from sandbox.api.resources.user import Registration, User, UserLogin, TokenRefresh, UserLogout
from sandbox.api.resources.category import Category, CategoriesList
from sandbox.api.resources.operation import Operation, OperationList
from . import api

api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")
api.add_resource(Registration, "/register")
api.add_resource(Category, "/category/<string:name>")
api.add_resource(CategoriesList, "/categories")
api.add_resource(User, "/user/<int:user_id>")
api.add_resource(UserLogin, "/login")
api.add_resource(TokenRefresh, "/refresh")
api.add_resource(UserLogout, "/logout")
api.add_resource(OperationList, "/user_operations")
api.add_resource(Operation, "/operation")


