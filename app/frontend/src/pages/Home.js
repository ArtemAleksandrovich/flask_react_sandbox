import React from 'react'

const  Home = () => {
    return(
        <div className="jumbotron jumbotron-fluid">
            <div className="container">
                <h1 className="display-4">Information</h1>
                <p className="lead">This is educational sandbox for practice FLASK backend bounded with REACT frontend</p>
            </div>
        </div>
    )
}

export default Home