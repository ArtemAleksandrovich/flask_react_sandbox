from sandbox.models.db import Base, db
from .mixin import SaveDeleteMixin, FindInstanceMixin


class UserModel(Base, db.Model, SaveDeleteMixin, FindInstanceMixin):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), nullable=False, unique=True)
    password = db.Column(db.String(16), nullable=False)
    operations = db.relationship(
        "OperationModel", cascade="all,delete", lazy="dynamic", back_populates="user"
    )

