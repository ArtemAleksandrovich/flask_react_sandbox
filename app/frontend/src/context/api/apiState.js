import React, {useReducer} from 'react'
import {
    GET_CATEGORIES,
    CLEAR_CATEGORIES,
    SET_LOADING,
    GET_ITEMS,
    CLEAR_ITEMS,
    GET_CATEGORY,
    GET_ITEM,
    CLEAR_ITEM,
    TRACK_ERROR,
    CLEAR_ERROR,
    DELETE_ITEM,
    SHOW_ITEMS,
    TRACK_RESPONSE,
    CLEAR_RESPONSE, DELETE_CATEGORY
} from "../types"
import axios from "axios"
import {apiReducer} from "./apiReducer"
import {apiContext} from "./apiContext"
import axiosToken from "../../axios/axiosToken";

const url = 'http://localhost:3000/api'

const  ApiState = ({children}) => {
    const initialState = {
        category: {},
        categories: [],
        loading: false,
        items: [],
        item: '',
        er: '',
        response: '',
        show: false
    }

    const [state, dispatch] = useReducer(apiReducer, initialState)

    const getCategories = () => {

        setLoading()
        // const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjI2MzM1MjU4LCJqdGkiOiI0ZjAwYjZmZS01YWM3LTQxYzYtOWNhNC00ZmM4OGRlOTRiMzciLCJ0eXBlIjoiYWNjZXNzIiwiaWRlbnRpdHkiOjEsIm5iZiI6MTYyNjMzNTI1OCwiZXhwIjoxNjI2MzM4ODU4LCJpc19hZG1pbiI6dHJ1ZX0.Hi4L_TyVFAOWhJCU5GHukr1Jh3vaQOyeMpTULx3mILc"
        // axios.get(`${url}/categories`, {headers:{
        //         'Content-type': 'application/json',
        //         'Authorization': `Bearer ${token}`
        //     }} )
        // debugger
        axiosToken.get('/categories')
            .then( response => {
                if (response.data.categories.length) {
                    dispatch({
                        type: GET_CATEGORIES,
                        payload: response.data.categories
                    })
                } else {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: 'No categories in database'
                    })
                }
            })
            .catch(error => {
                // debugger
                if (error.response) {
                    dispatch({
                        type: TRACK_ERROR,
                        payload: error.response.statusText
                    })
                }
            })
    }

    const getCategory = async (name) => {

        setLoading()

        const response = await axios.get(`${url}/category/${name}`)

        dispatch({
            type: GET_CATEGORY,
            payload: response.data
        })
    }


    const getItems = () => {

        setLoading()

        axios.get(`${url}/items`)
            .then( response => {
                if (response.data.items.length) {
                    dispatch({
                        type: GET_ITEMS,
                        payload: response.data.items
                    })
                } else {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: 'No items in database'
                    })
                }
            })
            .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_ERROR,
                        payload: error.response.data
                    })
                }
            })
    }


    const getItem = (itemName) => {

        setLoading()

        axios.get(`${url}/item/${itemName}`)
            .then( response => {
                dispatch({
                    type: GET_ITEM,
                    payload: response.data
                })
            }).catch( error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_ERROR,
                        payload: error.response.data.message
                    })
                }
            })
    }

    const deleteItem = (itemName) => {
        axios.delete(`${url}/item/${itemName}`)
            .then(response => {
                dispatch({
                    type: DELETE_ITEM,
                    payload: response.data.message,
                    item:''
                })
            })
            .catch( error => {
                dispatch({
                    type: TRACK_ERROR,
                    payload: error.response.data.message
                })
            })
        getItems()
    }

    const deleteCategory = (categoryName) => {
        axios.delete(`${url}/category/${categoryName}`)
            .then( response => {
                dispatch({
                    type: DELETE_CATEGORY,
                    payload: {
                        response: response.data.message,
                        categories: state.categories.filter((item) => item.name !== categoryName)
                    }
                    // ,
                })
            })
            .catch( error => {
                dispatch({
                    type: TRACK_ERROR,
                    payload: error.response.data.message
                })
            })
        getCategories()
    }


    const clearItem = () => dispatch({type: CLEAR_ITEM})
    const clearCategories = () => dispatch({type: CLEAR_CATEGORIES})
    const setLoading = () => dispatch({type: SET_LOADING})
    const clearError = () => dispatch({type: CLEAR_ERROR})
    const showItems = () => dispatch({type: SHOW_ITEMS})
    const clearResponse = () => dispatch({type: CLEAR_RESPONSE})
    const clearItems = () => dispatch({type: CLEAR_ITEMS})

    const {category, categories, items, loading, item, show, er, response} = state

    return(
        <apiContext.Provider value={{category, categories, items, loading, item, showItems, er, show, response,
            setLoading, clearCategories, getCategories, getItems, getCategory, getItem, clearItem, clearError,
            deleteItem, clearResponse, deleteCategory, clearItems}}>
            {children}
        </apiContext.Provider>
    )
}

export default ApiState