from flask import send_from_directory
from flask import current_app as app


@app.route("/static/<path:filename>")
def staticfiles(filename):
    return send_from_directory('/static', filename)
