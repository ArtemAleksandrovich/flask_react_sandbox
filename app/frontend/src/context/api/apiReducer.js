import {
    CLEAR_CATEGORIES, CLEAR_ERROR,
    CLEAR_ITEM,
    CLEAR_ITEMS, CLEAR_RESPONSE, DELETE_CATEGORY, DELETE_ITEM,
    GET_CATEGORIES,
    GET_CATEGORY,
    GET_ITEM,
    GET_ITEMS,
    SET_LOADING, SHOW_ITEMS,
    TRACK_ERROR, TRACK_RESPONSE
} from "../types";

const handlers = {
    [GET_CATEGORIES]: (state, {payload}) => ({...state, categories:payload, loading: false}),
    [SET_LOADING]: state => ({...state, loading: true}),
    [CLEAR_CATEGORIES]: state => ({...state, categories:[]}),
    [GET_ITEMS]: (state, {payload}) => ({...state, items:payload, loading: false, response: ''}),
    [CLEAR_ITEMS]: state => ({...state, items:[]}),
    [GET_CATEGORY]: (state, {payload}) => ({...state, category:payload, loading: false}),
    [GET_ITEM]: (state, {payload}) => ({...state, item:payload, loading: false, show: false}),
    [CLEAR_ITEM]: state => ({...state, items:[], item:''}),
    [TRACK_ERROR]: (state, {payload}) => ({...state, er: payload, loading: false}),
    [CLEAR_ERROR]:state => ({...state, er:''}),
    [DELETE_ITEM]: (state, {payload}) => ({...state, response: payload, item:'', loading: false, items: []}),
    [DELETE_CATEGORY]: (state, {payload}) => ({...state, item:'', ...payload, loading: false, items: []}),
    [SHOW_ITEMS]: state => ({...state, show: !state.show}),
    [TRACK_RESPONSE]: (state, {payload}) => ({...state, response: payload, items: [], loading: false, show: false}),
    [CLEAR_RESPONSE]: state => ({...state, response: ''}),
    DEFAULT: state => state
}

export const apiReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}