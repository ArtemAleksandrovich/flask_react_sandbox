from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from redis import Redis
from sqlalchemy import create_engine
from sandbox.config import Config

Base = declarative_base()
engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
db = SQLAlchemy()
REDIS_BLACKLIST = Redis(host="redis", port=6379)