import React from 'react';
import classes from './Loader.module.scss'

const Loader = () => (<div className={classes.center}>
    <div className="spinner-border" role="status">
        <span className="visually-hidden">Loading...</span>
    </div>
</div>)

export default Loader