import {
    ADD_CATEGORY,
    ADD_ITEM, ADD_USER, AUTH_SUCCESS,
    CHANGE_INPUT_VALUE,
    TRACK_RESPONSE,
    UPDATE_ITEM, USER_LOGIN,
} from "../types";

const handlers = {
    [ADD_ITEM]: (state, {payload}) => ({...state,...payload, response: 'item created'}),
    [ADD_USER]: (state, {payload}) => ({...state,...payload, response: 'user created'}),
    [AUTH_SUCCESS]: (state, {payload}) => {

        const newState = {...state, ...payload, response: 'successfully logged in', isLogin: true}
        return newState
    },
    [UPDATE_ITEM]: (state, {payload}) => ({...state,...payload, response: 'item updated'}),
    [CHANGE_INPUT_VALUE]: (state, {payload, error}) => {
        // debugger
        if (error.hasOwnProperty('categoryName')) {
            return {...state, ...payload, categoryFormErrors: {...error}, response: ""}
        }

        if (error.hasOwnProperty('userName') || error.hasOwnProperty('userPassword')) {
            return {...state, ...payload, loginFormErrors: {...error}, response: ""}
        }

        return {...state, ...payload, formErrors: {...state.formErrors, ...error}, response: ""}

    },
    [TRACK_RESPONSE]: (state, {payload}) => ({...state, response: payload}),
    [ADD_CATEGORY]: state => ({...state, response: 'category created'}),
    DEFAULT: state => state
}

const FormReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT
    return handler(state, action)
}

export default FormReducer