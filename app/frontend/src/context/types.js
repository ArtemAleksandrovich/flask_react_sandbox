//============================== API (GET/ DELETE)
export const SET_LOADING = 'SET_LOADING'
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const CLEAR_CATEGORIES = 'CLEAR_CATEGORIES'
export const GET_ITEMS = 'GET_ITEMS'
export const CLEAR_ITEMS = 'CLEAR_ITEMS'
export const GET_CATEGORY = 'GET_CATEGORY'
export const GET_ITEM = 'GET_ITEM'
export const CLEAR_ITEM = 'CLEAR_ITEM'
export const TRACK_ERROR = 'TRACK_ERROR'
export const CLEAR_ERROR = 'CLEAR_ERROR'
export const DELETE_ITEM = 'DELETE_ITEM'
export const DELETE_CATEGORY = 'DELETE_CATEGORY'
export const SHOW_ITEMS = 'SHOW_ITEMS'
export const TRACK_RESPONSE = 'TRACK_RESPONSE'
export const CLEAR_RESPONSE = 'CLEAR_RESPONSE'

//============================== FORM (POST, PUT)
export const ADD_ITEM = 'ADD_ITEM'
export const ADD_CATEGORY = 'ADD_CATEGORY'
export const CHANGE_INPUT_VALUE = 'CHANGE_INPUT_VALUE'
export const UPDATE_ITEM = 'UPDATE_ITEM'
export const ADD_USER = 'ADD_USER'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'


//============================== ALERT
export const HIDE_ALERT = 'HIDE_ALERT'
export const SHOW_ALERT = 'SHOW_ALERT'