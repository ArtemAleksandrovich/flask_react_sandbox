import React, {useEffect, useContext, Fragment} from 'react';
import {apiContext} from "../../context/api/apiContext";
import {Link} from "react-router-dom";
import Loader from "../ui/Loader";
import ItemCard from "./ItemCard";
import EditItem from "../forms/EditItem";


const Item = ({match}) => {
    const {item, loading, getItem } = useContext(apiContext)
    const itemName = match.params.name

    useEffect(() => {
        getItem(itemName)
    }, [])


    if (loading) {
        return <Loader/>
    }

    return(
        <Fragment>
            <Link to="/info" className="btn btn-link">Back to INFO</Link>
            <br/><hr/><br/>
            { match.path.includes('edit')
                ? <EditItem item={item}/>
                : <ItemCard item={item}/>
            }
        </Fragment>
    )
}

export default Item