import React, {useContext, useEffect} from 'react';
import {formContext} from "../../context/form/formContext";
import {apiContext} from "../../context/api/apiContext";
import {alertContext} from "../../context/alert/alertContext";
import {Link} from "react-router-dom";
import "./errorInput.css"


const LoginRegistration = () => {

    const {isLogin, handleInput, loginFormErrors,
        response, clearResponse, userName, userPassword, addUser, authUser} = useContext(formContext)
    const alert = useContext(alertContext)
    const state = useContext(formContext)

    const onChange = event => {
        handleInput(event)
    }

    useEffect(() => {
        if (response) {
            alert.show(response, 'warning')
            clearResponse()
            // clearCategories()
        }
        // if (isLogin) {
        //
        // }

    }, [response, loginFormErrors, userName, userPassword, isLogin])


    const submitHandler = event => {
        event.preventDefault()
        addUser()
    }

    const onClickHandler = event => {
        event.preventDefault()
        authUser()
    }

    // debugger
    const isEnabled = !Object.keys(loginFormErrors).some(field => loginFormErrors[field])

    return (
        <>
            <Link to="/info" className="btn btn-link">Back to INFO</Link>
            <br/>
            <hr/>
            <br/>
            <div>
                <form className="demoForm" onSubmit={submitHandler}>
                    <h2>Login/Registration form</h2>
                    <div className="form-group">
                        <label>User name</label>
                        <input
                            placeholder={'Enter your name (should contains letters, numbers or "-")'}
                            type="text"
                            className={`form-control ${loginFormErrors.userName ? "error" : ""}`}
                            name="userName"
                            onChange={onChange}
                            value={userName}
                        />
                        <span style={{color: 'red'}}> {loginFormErrors.userName || ""}</span>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input
                            placeholder={'Enter your password (should contains letters, numbers or "-")'}
                            type="password"
                            className={`form-control ${loginFormErrors.userPassword ? "error" : ""}`}
                            name="userPassword"
                            onChange={onChange}
                            value={userPassword}
                        />
                        <span style={{color: 'red'}}> {loginFormErrors.userPassword || ""}</span>
                    </div>
                    <br/>
                    <button disabled={!isEnabled || !userName || !userPassword } onClick={onClickHandler} className="btn btn-primary">Enter</button>
                    <button disabled={!isEnabled || !userName || !userPassword} type="submit" className="btn btn-outline-primary">Registration</button>
                </form>
            </div>
        </>
    )
}
export default LoginRegistration