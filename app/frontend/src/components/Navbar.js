import React from 'react'
import {NavLink} from "react-router-dom"

const Navbar = () => {
    return(
        <nav className={'navbar navbar-dark bg-primary navbar-expand-lg'}>
                <NavLink exact to="/" className={'navbar-brand'}>Flask Sandbox v2</NavLink>
            <ul className={'navbar-nav'}>
                <li className={'nav-item'}>
                    <NavLink exact to="/info" className={'nav-link'}>Api</NavLink>
                </li>
                <li className={'nav-item'}>
                    <NavLink exact to="/search" className={'nav-link'}>Search</NavLink>
                </li>
                <li className={'nav-item'}>
                    <NavLink exact to="/auth" className={'nav-link'}>Login/Registration</NavLink>
                </li>
                <li className={'nav-item'}>
                    <NavLink exact to="/logout" className={'nav-link'}>Logout</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar