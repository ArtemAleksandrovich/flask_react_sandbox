import React, {useContext, useEffect, useState} from 'react'
import {formContext} from "../../context/form/formContext"
import {apiContext} from "../../context/api/apiContext";
import Loader from "../ui/Loader";
import "./errorInput.css"
import {alertContext} from "../../context/alert/alertContext";
import {Link} from "react-router-dom";

const  AddItemForm = () => {
    const {handleInput, addItem, itemName,
        itemPrice, categoryId, formErrors, response, clearResponse} = useContext(formContext)
    const {categories, getCategories, showItems} = useContext(apiContext)

    const alert = useContext(alertContext)


    const onChange = event => {
        handleInput(event)
    }

    useEffect(() => {
        if (!categories.length) {
            getCategories()
        }
        if (response) {
            alert.show(response, 'warning')
            clearResponse()
            showItems()
        }

    },[categories, itemName, itemPrice, response])


    const submitHandler = event => {
        event.preventDefault()
        addItem()
    }
    const isEnabled = !Object.keys(formErrors).some(field => formErrors[field])

    return(
        <>
        <Link to="/info" className="btn btn-link">Back to INFO</Link>
        <br/><hr/><br/>
        <div>
            { categories.length
            ? (<form className="demoForm" onSubmit={submitHandler}>
                <h2>Add item</h2>
                <div className="form-group">
                    <label>Item name</label>
                    <input
                        placeholder={'Enter name (should contains letters, numbers or "-")'}
                        type="text"
                        className={`form-control ${formErrors.itemName ? "error" : ""}`}
                        name="itemName"
                        onChange={onChange}
                        value={itemName}
                    />
                    <span style={{color:'red'}}> {formErrors.itemName || ""}</span>
                </div>
                <div className="form-group">
                    <label>Item price</label>
                    <input type="text"
                           placeholder={'format "15.99"'}
                           className={`form-control ${formErrors.itemName ? "error" : ""}`}
                           name="itemPrice"
                           onChange={onChange}
                           value={itemPrice}
                    />
                    <span style={{color:'red'}}> {formErrors.itemPrice || ""}</span>
                </div>
                <div className="form-group">
                    <label>Select category</label>
                    <select name="categoryId"
                            className="form-select"
                            value={categoryId}
                            onChange={handleInput}>
                        <option value={""}>select category</option>
                         {categories.map((item, idx) => <option key={idx} value={item.id.toString()}>{item.name}</option>)}
                    </select>
                </div>
                <br/>
                <button disabled={!isEnabled} type="submit" className="btn btn-success">ADD ITEM</button>
            </form>)
            : <Loader/>
            }
        </div>
        </>
    )
}

export default AddItemForm