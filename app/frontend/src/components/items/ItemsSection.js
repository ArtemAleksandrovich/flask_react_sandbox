import React, {useContext, useEffect} from 'react'
import ItemThumbnail from "./ItemThumbnail";
import {apiContext} from "../../context/api/apiContext";
import {Link} from "react-router-dom";

const ItemsSection = () => {

    const {loading, items, getItems, showItems, clearItems, show} = useContext(apiContext)

    //TODO: remove
    const state = useContext(apiContext)



    useEffect(() =>{

        if (!items.length && show) {
            getItems()
        } else {
            clearItems()
        }
    },[show])

    return(
        <div>
            <div className="container">
                <h2 className="lead">Get items list</h2>
                <p><code>{"GET:<url>/api/items"}</code></p>
                <br/>
                <h2 className="lead">Get item by name</h2>
                <p><code>{"GET:<url>/api/item/<name>"}</code></p>
                <br/>
                <h2 className="lead">Delete item by name</h2>
                <p><code>{"DELETE:<url>/api/item/<name>"}</code></p>
                <br/>
                <h2 className="lead">Add item</h2>
                <p><code>{"POST:<url>/api/item/<name>"}</code></p>
                <br/>
                <div className={'btn btn-group'}>
                    <button onClick={showItems} className={'btn btn-primary'}>SHOW ITEMS</button>
                    <Link to={`/add/item`} className={'btn btn-success'}>ADD ITEM</Link>
                </div>
            </div>

            <div>
                <ul className="list-group">
                {
                    loading || !show
                    ? null
                    :  items.map(item =>
                            (<div key={item.id} className="col-sm-4 mb-4"><ItemThumbnail item={item}/></div>))
                }
                </ul>
            </div>
        </div>
    )
}

export default ItemsSection