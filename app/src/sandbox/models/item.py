from sandbox.models.db import Base, db
from .mixin import SaveDeleteMixin, FindInstanceMixin


class ItemModel(Base, db.Model, SaveDeleteMixin, FindInstanceMixin):
    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False, unique=True)
    price = db.Column(db.Float(precision=2), nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey("categories.id"), nullable=False)
    category = db.relationship("CategoryModel", back_populates="items")



