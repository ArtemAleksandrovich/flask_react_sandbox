from sandbox.ma import ma
from sandbox.models.item import ItemModel
from sandbox.models.category import CategoryModel


class ItemSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemModel
        load_only = ("category",)
        dump_only = ("id",)
        include_fk = True
        load_instance = True



