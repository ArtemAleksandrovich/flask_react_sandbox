import React, {useReducer} from 'react'
import {alertContext} from "./alertContext";
import AlertReducer from "./reducer";
import {HIDE_ALERT, SHOW_ALERT} from "../types";

const AlertState = ({children}) => {

    const [state, dispatch] = useReducer(AlertReducer, null)

    const hide = () => dispatch({type:HIDE_ALERT, text:""})
    const show = (text, type='secondary') => {
        setTimeout(() =>{
            dispatch({type:HIDE_ALERT, text:""})
        }, 3000)
        if (text) {
            dispatch({type:SHOW_ALERT, payload:{type, text}})
        }
    }

    return(
        <div>
            <alertContext.Provider value={{
                hide, show, alert:state
            }}>
                {children}
            </alertContext.Provider>
        </div>
    )
}

export default AlertState