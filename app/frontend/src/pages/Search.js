import React, {useContext, useEffect} from 'react'
import {apiContext} from "../context/api/apiContext"
import Loader from "../components/ui/Loader"
import ItemCard from "../components/items/ItemCard";
import FindItem from "../components/search/FindItem";
import {alertContext} from "../context/alert/alertContext";

const  Search = () => {

    const {loading, item, clearItem, er, clearError} = useContext(apiContext)
    const alert = useContext(alertContext)

    useEffect(() => {
        clearItem()
        if (er) {
            alert.show(er, 'warning')
            clearError()
        }
    }, [er])


    return(
        <React.Fragment>
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">Search item</h1>
                    <br/><hr/><br/>
                </div>
            </div>
            <FindItem/>
            {
                loading ? <Loader/> :
                item ? (<div><ItemCard item={item}/></div>) : null
            }
        </React.Fragment>
    )
}

export default Search