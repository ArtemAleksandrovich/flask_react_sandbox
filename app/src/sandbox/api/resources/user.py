from flask_restful import Resource
from flask import jsonify, request
from sandbox.models.db import REDIS_BLACKLIST
from sandbox.models.user import UserModel
from hmac import compare_digest
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    get_jwt,
)
from sandbox.config import Config
from sandbox.api.schemas.user import UserSchema
from .messages import *

user_schema = UserSchema()


class Registration(Resource):
    def post(self):
        user = user_schema.load(request.get_json())

        if UserModel.find_by_name(user.username):
            return {"message": USER_ALREADY_EXISTS}, 400

        user.save_to_db()
        return {"message": SUCCESS_CREATION.format(self.__class__.__name__)}, 201


class User(Resource):
    @classmethod
    def get(cls, user_id):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": NOT_FOUND_ERROR.format(cls.__name__)}, 404
        return user_schema.dump(user) or (NOT_FOUND_ERROR.format(cls.__name__), 404)

    @classmethod
    def delete(cls, user_id):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": NOT_FOUND_ERROR.format(cls.__name__)}, 404
        user.delete_from_db()
        return {"message": DELETED.format(cls.__name__)}


class UserLogin(Resource):
    @staticmethod
    def post():

        login_data = user_schema.load(request.get_json())
        user = UserModel.find_by_name(login_data.username)

        if user and compare_digest(user.password, login_data.password):
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)
            return {"access_token": access_token, "refresh_token": refresh_token}, 200
        return {"message": INVALID_CREDENTIALS}, 401


class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": new_token}, 200


class UserLogout(Resource):
    @jwt_required()
    def get(self):
        jti = get_jwt()["jti"]
        try:
            REDIS_BLACKLIST.set(jti, "", ex=Config.JWT_ACCESS_TOKEN_EXPIRES)
        except:
            return {"message": CREATE_ERROR}, 500
        return jsonify(message=LOGGED_OUT)
