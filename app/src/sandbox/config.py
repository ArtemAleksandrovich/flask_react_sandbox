from os import getenv, path
from datetime import timedelta
basedir = path.abspath(path.dirname(path.dirname(__file__)))


class Config:
    SQLALCHEMY_DATABASE_URI = getenv("DATABASE_URL", "sqlite:///" + path.join(basedir, "data.db"))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = True
    JWT_SECRET_KEY = getenv("JWT_SECRET_KEY", "replace_me")
    JWT_IDENTITY_CLAIM = "identity"
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)
    STATIC_FOLDER = f"{getenv('APP_FOLDER')}/static"
    JWT_TOKEN_LOCATION = "headers"