from flask_jwt_extended import JWTManager
from flask import jsonify
from sandbox.models.db import REDIS_BLACKLIST


jwt = JWTManager()


@jwt.additional_claims_loader
def add_claims_to_jwt(identity):
    if identity == 1:
        return {"is_admin": True}
    return {"is_admin": False}


@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload):
    jti = jwt_payload["jti"]
    token_in_redis = REDIS_BLACKLIST.get(jti)
    return token_in_redis is not None


@jwt.expired_token_loader
def expired_token_callback(jwt_headers, jwt_payload):
    return jsonify(description="The token has expired", error="token_expired"), 401


@jwt.invalid_token_loader
def invalid_token_callback(error):
    return (
        jsonify({"description": "Verification failed", "error": "invalid token"}),
        401,
    )


@jwt.unauthorized_loader
def unauthorized_token_callback(error):
    return (
        jsonify(
            {
                "description": "Request does not contain an access token.",
                "error": "authorization_required",
            }
        ),
        401,
    )


@jwt.needs_fresh_token_loader
def token_not_fresh_callback(jwt_headers, jwt_payload):
    return (
        jsonify(
            {"description": "The token is not fresh.", "error": "fresh_token_required"}
        ),
        401,
    )


@jwt.revoked_token_loader
def revoked_token_callback(jwt_headers, jwt_payload):
    return (
        jsonify(
            {"description": "The token has been revoked.", "error": "token_revoked"}
        ),
        401,
    )