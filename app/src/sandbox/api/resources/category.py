from flask_restful import Resource
from sandbox.models.category import CategoryModel
from sandbox.api.schemas.category import CategorySchema
from .messages import *
from flask_jwt_extended import jwt_required, get_jwt, get_jwt_identity


category_schema = CategorySchema()
category_list_schema = CategorySchema(many=True)


class Category(Resource):
    def get(self, name):
        category = CategoryModel.find_by_name(name)
        if category:
            return category_schema.dump(category)
        return {'message': NOT_FOUND_ERROR.format(self.__class__.__name__)}, 404

    def post(self, name):
        if CategoryModel.find_by_name(name):
            return {'message': INSTANCE_EXISTS.format(self.__class__.__name__)}, 400

        category = CategoryModel(name=name)
        try:
            category.save_to_db()
        except:
            return {"message": CREATE_ERROR}, 500

        return category_schema.dump(category), 201

    def delete(self, name):
        category = CategoryModel.find_by_name(name)
        if category:
            category.delete_from_db()

        return {'message': DELETED.format(self.__class__.__name__)}


class CategoriesList(Resource):
    @jwt_required()
    def get(self):
        return {'categories': category_list_schema.dump(CategoryModel.find_all())}
