import React, {useReducer} from 'react'
import {
    ADD_CATEGORY,
    ADD_ITEM, ADD_USER, AUTH_SUCCESS,
    CHANGE_INPUT_VALUE,
    TRACK_RESPONSE, UPDATE_ITEM, USER_LOGIN
} from "../types"

import axios from "axios"
import {formContext} from "./formContext";
import FormReducer from "./reducer";
import axiosToken from "../../axios/axiosToken";


const url = 'http://localhost:3000/api'

const  FormState = ({children}) => {
    const initialState = {
        itemName: '',
        itemPrice: '',
        categoryName: '',
        categoryId: '',
        formErrors: {itemName: false, itemPrice: false, categoryId: true},
        categoryFormErrors : {categoryName: false,},
        loginFormErrors: { userName: false, userPassword: false},
        userName: '',
        userPassword: '',
        response: '',
        isLogin: false
    }

    const [state, dispatch] = useReducer(FormReducer, initialState)

    const fieldValidation = (name, value) => {
        const priceRegex = /^\d+(?:\.?\d{2})?$/
        const nameRegex = /[^a-zA-Z0-9-]+/

        // debugger

        if (name.includes('Name') && nameRegex.test(value)){
              return false
          }
        else if (name.includes('Price') && !priceRegex.test(value)) {
              return false
          }
        else if (name.includes('categoryId') && !value) {
            return false
        }
        else if (name.includes('Password') && nameRegex.test(value)){
              return false
        }
        return true
    }


    const handleInput = event => {
        const name = event.target.name
        const value = event.target.value
        // debugger
        const error = fieldValidation(name, value) ? "" : `invalid ${name} input`
        dispatch({
            type: CHANGE_INPUT_VALUE,
            payload:{[name]: value},
            error: {[name]: error}
        })
    }

    const addCategory = () => {
        axios.post(`${url}/category/${state.categoryName}`)
        .then( response => {
            if (response.status === 201) {
                dispatch({
                    type: ADD_CATEGORY,
                    payload: initialState,
                })
            }
        })
        .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: error.response.data.message
                    })
                }
            })
    }



    const addItem = () => {
        axios.post(`${url}/item/${state.itemName}`, {
            price: state.itemPrice,
            category_id: state.categoryId
        })
        .then( response => {
            if (response.status === 201) {
                dispatch({
                    type:ADD_ITEM,
                    payload: initialState,
                })
            }
        })
        .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: error.response.data.message
                    })
                }
            })
    }

    const putItem = (item) => {
        const {name} = item
        axios.put(`${url}/item/${name}`, {
            price: state.itemPrice,
        })
        .then(response => {
            if (response.status === 200) {
                dispatch({
                    type:UPDATE_ITEM,
                    payload: initialState
                })
            }
        })
        .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: error.response.data.message
                    })
                }
            })
    }

    const addUser = () => {
        axiosToken.post('/register', {
            username: state.userName,
            password: state.userPassword
        })
        .then( response => {
            if (response.status === 201) {
                dispatch({
                    type: ADD_USER,
                    payload: initialState,
                })
            }
        })
        .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: error.response.data.message
                    })
                }
            })
    }



    const authUser = () => {
        axiosToken.post('/login', {
            username: state.userName,
            password: state.userPassword
        })
        .then( response => {
            saveTokenInformation(response.data.access_token, 'access')
            saveTokenInformation(response.data.refresh_token, 'refresh')
            if (response.status === 200) {
                dispatch(authSuccess())
                // dispatch({
                //     type: AUTO_LOGOUT
                // })
            }
        })
        .catch(error => {
                if (error.response) {
                    dispatch({
                        type: TRACK_RESPONSE,
                        payload: error.response.data.message
                    })
                }
            })
    }

    function authSuccess() {
        return {
            type: AUTH_SUCCESS
        }
    }

    function saveTokenInformation(jwtToken, type) {
        const {jwt, expirationDate} = getJwtData(jwtToken)
        // debugger
        localStorage.setItem(`${type}Token`, jwt)
        localStorage.setItem(`${type}ExpirationDate`, expirationDate)
    }

    function getJwtData(jwt) {
        const data = JSON.parse(atob(jwt.split('.')[1]))
        const expirationDate = new Date(data.exp * 1000)
        return {
            jwt, expirationDate
        }
    }



    const clearResponse = () => dispatch({type:TRACK_RESPONSE, payload:''})

    const {itemName, itemPrice, categoryId, categoryName, formErrors, response, categoryFormErrors, isLogin, loginFormErrors,
    userName, userPassword} = state


    return(
        <formContext.Provider value={{itemName, addItem, itemPrice, categoryId, categoryName, isLogin, loginFormErrors,
            handleInput, formErrors, response, putItem, clearResponse, addCategory,
            categoryFormErrors, userName, userPassword, addUser, authUser}}>
            {children}
        </formContext.Provider>
    )
}

export default FormState