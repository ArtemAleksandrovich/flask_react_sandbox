from sandbox.ma import ma
from sandbox.models.category import CategoryModel
from sandbox.api.resources.item import ItemSchema


class CategorySchema(ma.SQLAlchemyAutoSchema):
    items = ma.Nested(ItemSchema, many=True)

    class Meta:
        model = CategoryModel
        dump_only = ("id",)
        include_fk = True
        load_instance = True



