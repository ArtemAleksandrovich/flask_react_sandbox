import React, {useContext, useEffect} from 'react'
import {apiContext} from "../context/api/apiContext"
import Loader from "../components/ui/Loader"
import ItemsSection from "../components/items/ItemsSection"
import CategoriesSection from "../components/categories/CategoriesSection";
import {alertContext} from "../context/alert/alertContext";

const  API = () => {

    const {loading, response, clearResponse, er, clearError} = useContext(apiContext)
    const alert = useContext(alertContext)

    useEffect(() => {
        if (response) {
            alert.show(response, 'warning')
            setTimeout(() => {
                clearResponse()
            }, 3000)
        }
        if (er) {
            alert.show(er, 'warning')
            setTimeout(() => {
                clearError()
            }, 3000)
        }
    }, [response, er, clearResponse, clearError])

    return(
        <React.Fragment>
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">API method's</h1>
                    <br/><hr/><br/>
                </div>
            </div>
            <CategoriesSection/>
            <br/><hr/><br/>
            <ItemsSection/>
            {
                loading
                ? <Loader/>
                : null
            }
        </React.Fragment>
    )
}

export default API