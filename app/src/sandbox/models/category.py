from sandbox.models.db import Base, db
from .mixin import SaveDeleteMixin, FindInstanceMixin


class CategoryModel(Base, db.Model, SaveDeleteMixin, FindInstanceMixin):
    __tablename__ = "categories"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False, unique=True)
    items = db.relationship(
        "ItemModel", cascade="all,delete", lazy="dynamic", back_populates="category"
    )

