import React from 'react';
import Input from "./Input";

export const Form = ({object}) => {

    const objectFilelds = Object.entries(object)

    return (
        <div className="form-group">
            { objectFilelds.length
              ?  objectFilelds.map(field => <Input fieldName={field[0]} fieldValue={field[1]}/>)
              : null
            }
        </div>
    )
}