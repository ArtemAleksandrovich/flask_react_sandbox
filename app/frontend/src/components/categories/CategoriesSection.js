import React, {useContext, useEffect} from 'react';
import CategoryThumbnail from "./CategoryThumbnail";
import {apiContext} from "../../context/api/apiContext";
import {Link} from "react-router-dom";


const CategoriesSection = () => {

    const {loading, categories, getCategories} = useContext(apiContext)
    const api = useContext(apiContext)

    useEffect(() => {
        // if (!categories.length) {
        //     getCategories()
        // }
    }, [categories])

    return(
        <div>
            <div className="container">
                <h2 className="lead">Get categories list</h2>
                <p><code>{"GET:<url>/api/categories"}</code></p>
                <br/>
                <h2 className="lead">Get category by name</h2>
                <p><code>{"GET:<url>/api/category/<name>"}</code></p>
                <br/>
                <h2 className="lead">Add category</h2>
                <p><code>{"POST:<url>/api/category/<name>"}</code></p>
                <br/>
                <div className={'btn btn-group'}>
                    <button onClick={api.getCategories} className={'btn btn-primary'}>FETCH CATEGORIES</button>
                    <button onClick={api.clearCategories} className={'btn btn-warning'}>RESET</button>
                    <Link to={`/add/category`} className={'btn btn-success'}>ADD CATEGORY</Link>
                </div>
            </div>

            <div>
                <ul className="list-group">
                {
                    loading
                    ? null
                    :  categories.map(category =>
                            (<div key={category.id} className="col-sm-4 mb-4"><CategoryThumbnail category={category}/></div>))
                }
                </ul>

            </div>

        </div>
    )
}

export default CategoriesSection