import React, {useContext} from 'react'
import {alertContext} from "../context/alert/alertContext";

const Alert = () => {
    const {alert, hide} = useContext(alertContext)
    // console.log('payload from Alert', alert)
    if (!alert) return null

      return (
          <div className={`alert alert-${alert.type || 'secondary'} alert-dismissible fade show`} role="alert">
            {alert.text}
            <button onClick={hide} type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
      )
}

export default Alert