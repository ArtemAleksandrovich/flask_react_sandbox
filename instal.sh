#!/usr/bin/env bash

docker-compose -f ./deploy/docker-compose.prod.yml down
docker-compose -f ./deploy/docker-compose.prod.yml up --build -d
