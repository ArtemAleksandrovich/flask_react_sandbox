import React from "react"
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Navbar from "./components/Navbar";
import API from "./pages/API";
import Home from "./pages/Home";
import Alert from "./components/Alert";
import AlertState from "./context/alert/alertState";
import Category from "./components/categories/Category";
import ApiState from "./context/api/apiState";
import Item from "./components/items/Item";
import Search from "./pages/Search";
import FormState from "./context/form/formState";
import AddItem from "./components/forms/AddItem";
import AddCategory from "./components/forms/AddCategory";
import Logout from "./components/auth/Logout";

import Login from "./pages/Login";

function App() {
    return (
        <FormState>
            <ApiState>
                <AlertState>
                    <BrowserRouter>
                        <Navbar/>
                        <div className={"container pt-4"}>
                            <Alert/>
                            <Switch>
                                <Route path={'/info'} exact component={API}/>
                                <Route path={'/category/:name'} component={Category}/>
                                <Route path={'/item/edit/:name'} component={Item}/>
                                <Route path={'/item/:name'} component={Item}/>
                                <Route path={'/search'} component={Search}/>
                                <Route path={'/add/item'} component={AddItem}/>
                                <Route path={'/add/category'} component={AddCategory}/>
                                <Route path={'/auth'} component={Login}/>
                                <Route path={'/logout'} component={Logout}/>
                                <Route path={'/'} component={Home}/>
                            </Switch>
                        </div>
                    </BrowserRouter>
                </AlertState>
            </ApiState>
        </FormState>
    );
}

export default App;