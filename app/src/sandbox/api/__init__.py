from flask import Blueprint, jsonify
from flask_restful import Api
from marshmallow import ValidationError

api_blueprint = Blueprint('api', __name__,  url_prefix='/api')


@api_blueprint.errorhandler(ValidationError)
def handle_marshmallow_validation(error):
    return jsonify(error.messages), 400


api_blueprint.register_error_handler(ValidationError, handle_marshmallow_validation)
api = Api(api_blueprint)
from . import routes
