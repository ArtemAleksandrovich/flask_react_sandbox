import React, {useContext, useEffect} from 'react';
import {apiContext} from "../../context/api/apiContext";
import {Link} from "react-router-dom";


const ItemControls = ({itemName}) => {
    const {deleteItem} = useContext(apiContext)

    const onDelete = () => {
        deleteItem(itemName)
    }

    return(
        <>
            <Link to={`/item/${itemName}`} className={'btn btn-primary'}>view item</Link>
            <Link to={`/item/edit/${itemName}`} className={'btn btn-warning'}>edit item</Link>
            <button onClick={onDelete} className={'btn btn-danger'}>
                delete
            </button>
        </>
    )
}

export default ItemControls