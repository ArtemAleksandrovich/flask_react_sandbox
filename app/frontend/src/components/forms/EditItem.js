import React, {useContext, useEffect} from 'react';
import {formContext} from "../../context/form/formContext";
import {apiContext} from "../../context/api/apiContext";
import {alertContext} from "../../context/alert/alertContext";
import Loader from "../ui/Loader";
import './errorInput.css'


const EditItem = ({item}) => {
    const {handleInput, itemPrice, formErrors, putItem, response, clearResponse} = useContext(formContext)
    const {categories, getCategories} = useContext(apiContext)
    const alert = useContext(alertContext)

    const onChange = event => {
        handleInput(event)
    }

    useEffect(() => {
        if (!categories.length) {
            getCategories()
        }
        if (response) {
            alert.show(response, 'warning')
            clearResponse()
        }

    },[categories, item.price, response])


    const submitHandler = event => {
        event.preventDefault()
        putItem(item)
    }
    const isEnabled = !formErrors.itemPrice

    const itemCategory = categories.filter(category => category.id === item.category_id)[0]


    return(

        <div>
            { itemCategory
            ? (<form className="demoForm" onSubmit={submitHandler}>
                <h2>Edit item "{item.name}" - id {item.id} / price: {item.price}</h2>
                <div className="form-group">
                    <input readOnly
                        name="itemName"
                        className={`form-control ${formErrors.itemName ? "error" : ""}`}
                        value={item.name}
                        disabled={true}
                    />
                </div>

                <div className="form-group">
                    <input readOnly type="text"
                           value={itemCategory.name}
                           className={`form-control ${formErrors.itemName ? "error" : ""}`}
                           name="categoryId"
                    />
                </div>
                <div className="form-group">
                    <label>New price</label>
                    <input type="text"
                           placeholder={'format "15.99"'}
                           className={`form-control ${formErrors.itemName ? "error" : ""}`}
                           name="itemPrice"
                           onChange={onChange}
                           value={itemPrice}
                    />
                    <span style={{color:'red'}}> {formErrors.itemPrice || ""}</span>
                </div>
                <br/>
                <button disabled={!isEnabled} type="submit" className="btn btn-success">SAVE</button>
            </form>)
            : <Loader/>
            }
        </div>
    )
}


export default EditItem