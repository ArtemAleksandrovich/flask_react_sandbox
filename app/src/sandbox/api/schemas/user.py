from sandbox.ma import ma
from sandbox.models.user import UserModel
from sandbox.models.operation import OperationModel


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = UserModel
        load_only = ("password",)
        dump_only = ("id",)
        load_instance = True



