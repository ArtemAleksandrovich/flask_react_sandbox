import axios from "axios";

const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjI2MzQzMDEwLCJqdGkiOiIzMTYwNmJmYi0wYTJhLTQ5NTItYjE2Ny1iYjFmODgyNzk4MzciLCJ0eXBlIjoiYWNjZXNzIiwiaWRlbnRpdHkiOjEsIm5iZiI6MTYyNjM0MzAxMCwiZXhwIjoxNjI2MzQ2NjEwLCJpc19hZG1pbiI6dHJ1ZX0.Xf2y4si2t03VCaLk-E9gfoeL3Y17bEqjc25_ey4s47M"
// const token = ""

//TODO: add callback for getting token from localstorage
const axiosToken =  axios.create({
    baseURL: 'http://localhost:3000/api',
    headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
})

export default  axiosToken
