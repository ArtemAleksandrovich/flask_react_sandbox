import hashlib
from flask_restful import Resource, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from sandbox.models.operation import OperationModel
from sandbox.api.schemas.operation import OperationSchema
from .messages import *

operation_schema = OperationSchema()
operation_list_schema = OperationSchema(many=True)


class Operation(Resource):
    @jwt_required()
    def post(self):
        user_id = get_jwt_identity()
        row_operation = request.get_json()

        if OperationModel.find_operations(
            operation=row_operation['requested_operation'], user_id=user_id
        ):
            return {"message": INSTANCE_EXISTS.format(self.__class__.__name__)}, 400

        answer = hashlib.sha224(row_operation["requested_operation"].encode()).hexdigest()
        operation = operation_schema.load(row_operation)
        operation.answer = answer
        operation.user_id = user_id
        try:
            operation.save_to_db()
        except:
            return {"message": CREATE_ERROR}, 500
        return operation_schema.dump(operation), 201


class OperationList(Resource):
    @jwt_required()
    def get(self):
        user_id = get_jwt_identity()
        operations = OperationModel.find_operations(user_id=user_id)
        if operations:
            return {"operations": operation_list_schema.dump(operations)}, 200
        return {"message": NOT_FOUND_ERROR.format(self.__class__.__name__)}, 404
